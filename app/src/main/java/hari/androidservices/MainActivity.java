package hari.androidservices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    //views
    private TextView city;
    private LocalBroadcastManager localBroadcastManager;
    private IpApiReceiver ipApiReceiver;
    private IntentFilter mLoginIntentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //pull view
        city = (TextView) findViewById(R.id.city);
        initBrodcastReceivers();
        IpApiService.startIpApiService(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        localBroadcastManager.registerReceiver(ipApiReceiver, mLoginIntentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(ipApiReceiver);
    }

    private void initBrodcastReceivers() {
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        ipApiReceiver = new IpApiReceiver();
        mLoginIntentFilter = new IntentFilter(IpApiService.IP_API_SERVICE);
    }


    private class IpApiReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(IpApiService.IP_API_SERVICE)) {
                if (intent.getBooleanExtra(Constants.ERROR, false)) {
                    Log.e(TAG, "Successfully got from Ip Api");
                    IpApiResponseModel ipApiResponseModel = IpApiResponseModel.toPojo(intent.getStringExtra(Constants.RESULT));
                    city.setText(ipApiResponseModel.getCity());
                } else {
                    Log.e(TAG, "Failed to get location info from Ip Api");
                }
            }

        }
    }
}
